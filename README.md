<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-Web Matrix SDK</h2>
</div>

---

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst.

## Grundsätzliches

BundesMessenger-Web Matrix SDK ist ein unterstützendes Repository für BundesMessenger Web.
Es kapselt die Basisfunktionalitäten des Messengers und ist zwingend notwendig zur Erstellung des BundesMessenger Web.

## Repo

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-matrix-sdk

## Fehler und Verbesserungsvorschläge

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-matrix-sdk/-/issues

## Abhängigkeiten

[matrix-org/matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk)

[BundesMessenger Web - React SDK](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-react-sdk)

[BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web)

## Nutzung

Das Matrix JS SDK dient als Library für Javascript Clients und wird nicht eigenständig ausgeliefert.
Weitere Informationen können auch dem [matrix-org/matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk) Github Repo entnommen werden.

## Weitere Infos

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und alles weitere zur Web App findest Du unter [BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)
