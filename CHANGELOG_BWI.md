# Changes in BWI project 2.21.0 (2024-09-03)
Upstream merge ✨: 34.2.0

Bugfix 🐛:
- fix for CVE-2024-42369

# Changes in BWI project 2.17.0 (2024-04-29)
Upstream merge ✨: v31.4.0

# Changes in BWI project 2.16.0 (2024-03-21)
Upstream merge ✨: v31.3.0

# Changes in BWI project 2.15.0 (2024-02-12)
Upstream merge ✨: v30.3.0

# Changes in BWI project 2.14.0 (2024-01-15)
Upstream merge ✨: v30.2.0

# Changes in BWI project 2.12.0 (2023-12-4)
Upstream merge ✨: v30.0.1

# Changes in BWI project 2.11.0 (2023-10-24)
Upstream merge ✨: v29.0.0

Improvements 🙌:
- improved gitlab pipeline

# Changes in BWI project 2.10.0 (2023-09-26)
Upstream merge ✨: v28.2.0

Improvements 🙌:
- Readme improved

# Changes in BWI project 2.9.0 (2023-08-28)
Upstream merge ✨: v27.2.0

Improvements 🙌:
- pipeline set to use node 18

Features ✨:
- http client flag to block all backend requests except specific ones

# Changes in BWI project 2.8.0 (2023-07-31)
Upstream merge ✨: v26.1.0

Improvements 🙌:
- fixed pipeline

# Changes in BWI project 2.7.0 (2023-06-30)
Upstream merge ✨: v25.1.1

Improvements 🙌:
- Added show_participants flag to poll event

# Changes in BWI project 2.6.0 (2023-05-04)
Upstream merge ✨: v24.0.0

Improvements 🙌:
-   sonarqube pipeline improvement
-   jest pipeline improvement

# Changes in BWI project 2.5.0 (2023-04-11)
Upstream merge ✨: v23.5.0

Bugfix 🐛:
CVE-2023-28427 / GHSA-mwq8-fjpf-c2gr f

# Changes in BWI project 2.4.0 (2023-03-10)
Upstream merge ✨: v23.2.0
# Changes in BWI project 2.3.0 (2023-02-10)

Upstream merge ✨: v22.0.0

Bugfix 🐛: well-known request generated from base url in a correct way again

# Changes in BWI project 2.2.0 (2023-01-13)

Upstream merge ✨: v22.0.0

Improvements 🙌: Sonarqube reporter replaced

# Changes in BWI project 2.1.0 (2022-12-13)

Upstream merge ✨: v21.1.0

# Changes in BWI project 2.0.0 (2022-11-18)

Upstream merge ✨: v21.0.0

-   setPowerLevel changed to accept array of users as well
-   rewritten minor parts due to http-api migration

# Changes in BWI project 1.26.0 (2022-10-25)

Upstream merge ✨: v20.0.1

Bugfix 🐛:

-   CVE-2022-39249
-   CVE-2022-39250
-   CVE-2022-39251
-   CVE-2022-39236

Improvements 🙌:

-   changed project to use yarn link instead of workspaces due to compatibility issues

# Changes in BWI project 1.25.0 (2022-09-20)

Upstream merge ✨: v19.4.0

# Changes in BWI project 1.24.0 (2022-08-29)

Upstream merge ✨: v19.1.0

Bugfix 🐛:
CVE-2022-36059 (https://github.com/matrix-org/matrix-js-sdk/commit/8716c1ab9ba93659173b806097c46a2be115199f)

# Changes in BWI project 1.23.0 (2022-08-02)

Upstream merge ✨: v19.0.0

# Changes in BWI project 1.22.0 (2022-07-01)

Upstream merge ✨: v18.0.0

# Changes in BWI project 1.21.0 (2022-06-03)

Upstream merge ✨: v17.2.0
