import { ClientPrefix, Method, TypedEventEmitter } from "../../src";
import { FetchHttpApi } from "../../src/http-api/fetch";

describe("FetchHttpApi BWI Blocking", () => {
    const baseUrl = "http://baseUrl";
    const prefix = ClientPrefix.V3;

    const checkFetch = (
        api: FetchHttpApi<{ baseUrl: string; prefix: ClientPrefix; fetchFn: jest.Mock<any, any, any> }>,
        fetchFn: jest.Mock<any, any, any>,
    ) => {
        api.request(Method.Get, "/foo");
        api.request(Method.Get, "/baz");
        expect(fetchFn.mock.calls[0][0].href.endsWith("/foo")).toBeTruthy();
        expect(fetchFn.mock.calls[1][0].href.endsWith("/baz")).toBeTruthy();
    };

    it("should not block request when blocking not set", () => {
        const fetchFn = jest.fn().mockResolvedValue({ ok: true });
        const api = new FetchHttpApi(new TypedEventEmitter<any, any>(), { baseUrl, prefix, fetchFn });

        checkFetch(api, fetchFn);
    });

    it("should block requests when blocking set, and unblock after disabling blocking", () => {
        const fetchFn = jest.fn().mockResolvedValue({ ok: true });
        const api = new FetchHttpApi(new TypedEventEmitter<any, any>(), { baseUrl, prefix, fetchFn });
        api.enableBlocking();

        api.request(Method.Get, "/foo");
        api.request(Method.Get, "/baz");

        expect(fetchFn).not.toHaveBeenCalled();

        api.disableBlocking();

        checkFetch(api, fetchFn);
    });

    it("should not block if blocking enabled but avoidBlocking is set in request", () => {
        const fetchFn = jest.fn().mockResolvedValue({ ok: true });
        const api = new FetchHttpApi(new TypedEventEmitter<any, any>(), { baseUrl, prefix, fetchFn });
        api.enableBlocking();
        api.request(Method.Get, "/foo");
        api.request(Method.Get, "/baz");

        expect(fetchFn).not.toHaveBeenCalled();

        api.request(Method.Get, "/foo", undefined, undefined, { avoidBlocking: true });
        api.request(Method.Get, "/baz", undefined, undefined, { avoidBlocking: true });

        checkFetch(api, fetchFn);
    });
});
