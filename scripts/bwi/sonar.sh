# Copyright 2023 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

set -x

mv ./matrix-react-sdk/coverage coverage
COVERAGE=./coverage

# extract version number from package.json (ignore RCs)
APP_VERSION=`sed -n 's/.*"version":.*"\([0-9]*\.[0-9]*\.[0-9]*\)[-rc0-9\.]*".*/\1/p' package.json`

DEV_VERSION=$2

sonar-scanner \
    -Dsonar.projectKey=$CI_PROJECT_NAME:latest \
    -Dsonar.sources=src \
    -Dsonar.host.url=$SONAR_URL \
    -Dsonar.token=$DSONAR_LOGIN \
    -Dsonar.projectVersion=$APP_VERSION$DEV_VERSION \
    -Dsonar.python.version=3.10 \
    -Dsonar.qualitygate.wait=true \
    -Dsonar.javascript.lcov.reportPaths="$COVERAGE/lcov.info" \
    -Dsonar.testExecutionReportPaths="$COVERAGE/jest-sonar-report.xml" \
    -Dsonar.sonar.coverage.exclusions="spec/**/*" \
    -Dsonar.typescript.tsconfigPath=./tsconfig.json  \
    ${CI_MERGE_REQUEST_IID:+ -Dsonar.pullrequest.key=${CI_MERGE_REQUEST_IID}} \
    ${CI_MERGE_REQUEST_IID:+ -Dsonar.pullrequest.base=$CI_DEFAULT_BRANCH} \
    ${CI_MERGE_REQUEST_IID:+ -Dsonar.pullrequest.branch=${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}} || exit 0 \

    # The global report is allowed to fail.
    # Return 0 to indicate a success in the pipeline even if the scan fails.
    # The allow_failure setting on the job will do the same but marks the job result with a warning


